package com.company;

import java.util.*;

public class Main {

    public static void main(String[] args) {
	    ArrayList<Album> albumArrayList = new ArrayList<Album>();
        //I only need the String name for the Linked List, thats all I'm going to be getting, period.
        Album album1 = new Album("Eminem");
        Album album2 = new Album("JayZ");
        Album album3 = new Album("TheRock");

        album1.addSong("Song1", 2.32);
        album1.addSong("Song2", 2.35);
        album1.addSong("Song3", 3.43);
        album1.addSong("Song4", 1.49);


        albumArrayList.add(album1);
        //List Exists
        LinkedList<String> playList = new LinkedList<String>();
        addSongToPlaylist(playList, album1);
        addSongToPlaylist(playList, album1);
        addSongToPlaylist(playList, album1);
        addSongToPlaylist(playList, album1);
        printSongList(playList);
        //removeSongFromList(playList, album1);
        //printSongList(playList);

        //How do I add a song to it?


        playMusic(playList);


    }

    private static void playMusic(LinkedList<String> playList){
        Scanner scanner = new Scanner(System.in);
        boolean quit = false;
        boolean goingForward = true;
        //This creates the item that will let me run through the list, effectively it is the base level
        ListIterator<String> listIterator = playList.listIterator();
        //I dont need playlist because listErator is now an object which is equal to playlist etc.
        //From here on I need to verify that A) The List has Items and B) If so, which one I'm going to.

        if(playList.isEmpty()){
            System.out.println("This playlist is empty!");
        }else {
            System.out.println("Now playing " + listIterator.next());
            printMenu();
        }


        while(!quit){
            int action = scanner.nextInt();
            //This is what is used to clear, not .next();
            scanner.nextLine();
            switch(action){
                case 0:
                    System.out.println("Exiting Playlist!");
                    quit = true;
                    break;
                case 1:
                    if(!goingForward){
                        if(listIterator.hasNext()){
                            listIterator.next();
                        }
                        goingForward = true;
                    }
                    if(listIterator.hasNext()){
                        System.out.println("Now playing: " + listIterator.next());
                    }else{
                        System.out.println("End of Playlist");
                        goingForward = false;
                    }
                    break;
                case 2:
                    if(goingForward){
                        if(listIterator.hasPrevious()){
                            listIterator.previous();
                        }
                        goingForward = false;
                    }
                    if(listIterator.hasPrevious()){
                        System.out.println("Now Playing " + listIterator.previous());
                    }else {
                        System.out.println("Beginning of Playlist");
                        goingForward = true;
                    }
                    break;
                case 3:
                    printMenu();
                    break;
            }

        }

    }


    private static boolean addSongToPlaylist(LinkedList<String> linkedList, Album album){
        Scanner scanner = new Scanner(System.in);
        System.out.print("Please enter the song name to add: ");
        String songName = scanner.nextLine();


        if(album.firstFindSong(songName)){
            linkedList.add(songName);
            System.out.println(songName + " added to playlist");
            return true;
        }
            System.out.println("Song does not exist in that album!");
            return false;
    }

    private static void printSongList (LinkedList<String> linkedList){
        Iterator<String> i = linkedList.iterator();
        while(i.hasNext()){
            System.out.println("Song " + i.next());
        }
        System.out.println("==============================");

    }

    private static void removeSongFromList(LinkedList<String> linkedList, Album album){
        Scanner scanner = new Scanner(System.in);
        System.out.print("Please enter the song name to add: ");
        String songName = scanner.nextLine();

        if(album.firstFindSong(songName)){
            linkedList.remove(songName);
            System.out.println(songName + " removed from playlist");
        }
        System.out.println("Song does not exist in that album!");

    }

    private static void printMenu(){
        System.out.println("Menu Options:");
        System.out.println("Option  0 - Exit Playlist\n" +
                            "Option 1 - Next Song\n" +
                            "Option 2 - Previous Song\n" +
                            "Option 3 - Menu Options\n");
    }
}

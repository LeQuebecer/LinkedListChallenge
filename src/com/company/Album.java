package com.company;

import java.util.ArrayList;
import java.util.LinkedList;

public class Album {
    private String albumTitle;
    private double albumDuration;
    private  ArrayList<Song> songList;

    public Album(String albumTitle) {
        this.albumTitle = albumTitle;
        this.songList = new ArrayList<Song>();
    }

    //What functions might I need?
    //This doesn't need to be SUPER complicated
    //Add Song
    //Requirements - See if song already exists
    public boolean addSong(String songTitle, double songDuration){
        Song newSong = new Song(songTitle, songDuration);
        if(firstFindSong(songTitle)){
            return false;
        }
        this.songList.add(newSong);
        return true;
    }



    public boolean firstFindSong(String title){

        //Another method is checked<ArrayType>. This skips the need for the for Loop.
        //An alternative way of going through a list.
        //Known as for the forEach command, another way of writing it.
        for(int i = 0 ; i < this.songList.size(); i++){
            if(this.songList.get(i).getSongTitle().equalsIgnoreCase(title));
            return false;
        }
        return true;
    }


    public Song findSong(String title){

        //Another method is checked<ArrayType>. This skips the need for the for Loop.
        //An alternative way of going through a list.
        //Known as for the forEach command, another way of writing it.
        for(Song checkedSong: this.songList){
            if (checkedSong.getSongTitle().equalsIgnoreCase(title)){
                return checkedSong;
            }
        }
        return null;
    }


    //Create a playlist Method and do it right here
    //This is another addition - I think I did something similar in the Main.
    public boolean addToPlaylist(int trackNumber, LinkedList<Song> playList){
        int index = trackNumber - 1;
                if((index >=0) && (index <= this.songList.size())){
                    playList.add(this.songList.get(index));
                    return true;
                }

                System.out.println("This album does not have a track " + trackNumber);
                return false;
    }

    public boolean addToPlayList (String songTitle, LinkedList<Song> playList){
        Song checkedSong = findSong(songTitle);
        if((checkedSong != null)){
            playList.add((checkedSong));
            return true;
        }

        System.out.println("This album does not have a song named: " + checkedSong.getSongTitle());
        return false;
    }
}
